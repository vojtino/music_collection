<?php

namespace App\Components;

use App\Model\AlbumRepository;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;

final class AlbumFormControl extends Control
{
    /** @var AlbumRepository */
    public $albumRepository;

    /** @var object current album */
    private $album;

    /** @var array of possible artists */
    private $artists;

    public function setAlbum($album)
    {
        $this->album = $album;
    }

    public function __construct(AlbumRepository $albumRepository, $artists)
    {
        $this->albumRepository = $albumRepository;
        $this->artists = $artists;
    }

    public function render()
    {
        $this->template->render(__DIR__ . '/AlbumFormControl.latte');
    }

    public function renderEdit()
    {
        $form = $this['form'];
        $form->setDefaults($this->album);
        $form['submit']->caption = 'Save';

        $this->template->render(__DIR__ . '/AlbumFormControl.latte');
    }

    public function renderForArtist($artist)
    {
        $form = $this['form'];
        unset($form['artist_id']);
        $form->addHidden('artist_id', $artist->id);

        $this->template->render(__DIR__ . '/AlbumFormControl.latte');
    }

    public function createComponentForm()
    {
        $form = new Form();
        $form->addHidden('id');
        $form->addText('name', 'Name');
        $form->addSelect('artist_id', 'Artist', $this->artists);
        $form->addSubmit('submit', 'Add');

        $form->onSuccess[] = [$this, 'formSucceeded'];
        return $form;
    }

    public function formSucceeded(Form $form)
    {
        $values = $form->getValues();

        if ($values['id']) {
            $this->albumRepository->update($values);
        } else {
            $this->albumRepository->insert($values);
        }

        $this->presenter->flashMessage('Album has been saved', 'alert-success');

        $this->presenter->redirect("Homepage:default");
    }
}