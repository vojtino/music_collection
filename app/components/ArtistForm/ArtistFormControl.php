<?php

namespace App\Components;

use App\Model\ArtistRepository;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;

final class ArtistFormControl extends Control
{
    /** @var ArtistRepository */
    public $artistRepository;

    /** @var object current artist */
    private $artist;

    public function setArtist($artist)
    {
        $this->artist = $artist;
    }

    public function __construct(ArtistRepository $artistRepository)
    {
        $this->artistRepository = $artistRepository;
    }

    public function render()
    {
        $this->template->render(__DIR__ . '/ArtistFormControl.latte');
    }

    public function renderEdit()
    {
        $form = $this['form'];
        $form->setDefaults($this->artist);
        $form['submit']->caption = 'Save';

        $this->template->render(__DIR__ . '/ArtistFormControl.latte');
    }

    public function createComponentForm()
    {
        $form = new Form();
        $form->addHidden('id');
        $form->addText('name', 'Name');
        $form->addSubmit('submit', 'Add');

        $form->onSuccess[] = [$this, 'formSucceeded'];
        return $form;
    }

    public function formSucceeded(Form $form)
    {
        $values = $form->getValues();

        if ($values['id']) {
            $this->artistRepository->update($values);
        } else {
            $this->artistRepository->insert($values);
        }

        $this->presenter->flashMessage('Artist has been saved', 'alert-success');

        $this->presenter->redirect("Homepage:default");
    }
}