<?php

namespace App\Components;

use Nette\Security\User;
use Nette\Application\UI\Form;
use Nette\Application\UI\Control;
use App\Model\CredentialsAuthenticator;
use Nette\Security\AuthenticationException;

class LoginFormControl extends Control
{
    /** @var User  */
    private $user;

    /** @var CredentialsAuthenticator  */
    private $credentialsAuthenticator;

    public function __construct(User $user, CredentialsAuthenticator $credentialsAuthenticator)
    {
        $this->user = $user;
        $this->credentialsAuthenticator = $credentialsAuthenticator;
    }

    public function render()
    {
        $this->template->render(__DIR__ . '/LoginFormControl.latte');
    }

    protected function createComponentForm()
    {
        $form = new Form();
        $form->addText('username', 'Username:')
            ->setRequired('Please enter your username.');
        $form->addPassword('password', 'Password:')
            ->setRequired('Please enter your password.');
        $form->addSubmit('submit', 'Sign in');
        $form->onSuccess[] = [$this, 'formSucceeded'];
        return $form;
    }

    public function formSucceeded(Form $form)
    {
        $values = $form->getValues();

        try {
            $this->user->login($this->credentialsAuthenticator->authenticate($values->username, $values->password));
            $this->presenter->redirect('Homepage:Default');
        } catch (AuthenticationException $e) {
            $this->presenter->flashMessage($e->getMessage(), 'alert-warning');
        }

    }
}