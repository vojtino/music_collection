<?php

namespace App\Model;

use Nette\Database\Context;

final class AlbumRepository
{
    const TABLE_NAME = 'album';

    /** @var Context */
    private $database;

    public function __construct(Context $database)
    {
        $this->database = $database;
    }

    /**
     * @return \Nette\Database\Table\Selection
     */
    public function getAll()
    {
        return $this->database->table(self::TABLE_NAME);
    }

    /**
     * @param $id
     * @return false|\Nette\Database\Table\ActiveRow
     */
    public function getById($id)
    {
        return $this->database->table(self::TABLE_NAME)->where('id', $id)->fetch();
    }

    /**
     * @param $values
     * @return bool|int|\Nette\Database\Table\ActiveRow
     */
    public function insert($values)
    {
        return $this->database->table(self::TABLE_NAME)->insert([
            'name' => $values['name'],
            'artist_id' => $values['artist_id'],
        ]);
    }

    /**
     * @param $values
     * @return int
     */
    public function update($values)
    {
        return $this->database->table(self::TABLE_NAME)->where('id', $values['id'])->update([
            'name' => $values['name'],
            'artist_id' => $values['artist_id'],
        ]);
    }

    /**
     * @param $id
     * @return int
     */
    public function delete($id)
    {
        return $this->database->table(self::TABLE_NAME)->where('id', $id)->delete();
    }
}