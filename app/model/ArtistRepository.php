<?php

namespace App\Model;

use Nette\Database\Context;

final class ArtistRepository
{
    const TABLE_NAME = 'artist';

    /** @var Context */
    private $database;

    public function __construct(Context $database)
    {
        $this->database = $database;
    }

    /**
     * @return \Nette\Database\Table\Selection
     */
    public function getAll()
    {
        return $this->database->table(self::TABLE_NAME);
    }

    /**
     * @param $id
     * @return false|\Nette\Database\Table\ActiveRow
     */
    public function getById($id)
    {
        return $this->database->table(self::TABLE_NAME)->where('id', $id)->fetch();
    }

    /**
     * @param $values
     * @return bool|int|\Nette\Database\Table\ActiveRow
     */
    public function insert($values)
    {
        return $this->database->table(self::TABLE_NAME)->insert([
            'name' => $values['name'],
        ]);
    }

    /**
     * @param $values
     * @return int
     */
    public function update($values)
    {
        return $this->database->table(self::TABLE_NAME)->where('id', $values['id'])->update([
            'name' => $values['name'],
        ]);
    }

    /**
     * @param $id
     * @return int
     */
    public function delete($id)
    {
        return $this->database->table(self::TABLE_NAME)->where('id', $id)->delete();
    }
}