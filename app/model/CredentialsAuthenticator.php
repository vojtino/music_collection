<?php

namespace App\Model;

use Nette\Security\User;
use Nette\Database\Context;
use Nette\Security\Identity;
use Nette\Security\IAuthenticator;
use Nette\Security\AuthenticationException;
use Nette\Security\Passwords;

final class CredentialsAuthenticator
{
    /** @var User */
    private $user;

    /** @var Context */
    private $database;

    public function __construct(User $user, Context $database)
    {
        $this->user = $user;
        $this->database = $database;
    }

    /**
     * @param $username
     * @param $password
     * @return Identity
     * @throws AuthenticationException
     */
    public function authenticate($username, $password)
    {
        $row = $this->database->table('user')
            ->where('username', $username)
            ->fetch();
        if (!$row) {
            throw new AuthenticationException('Invalid credentials.', IAuthenticator::IDENTITY_NOT_FOUND);
        } elseif (!Passwords::verify($password, $row['password'])) {
            throw new AuthenticationException('Invalid credentials.', IAuthenticator::INVALID_CREDENTIAL);
        }
        $arr = $row->toArray();
        unset($arr['password']);
        return new Identity($row['id'], null, $arr);
    }
}