<?php

namespace App\Presenters;

use App\Model\AlbumRepository;
use App\Components\AlbumFormControl;
use App\Model\ArtistRepository;

final class AlbumPresenter extends SecuredPresenter
{
    /** @var ArtistRepository @inject */
    public $artistRepository;

    /** @var AlbumRepository @inject */
    public $albumRepository;

    public function renderShow($id)
    {
        $album = $this->albumRepository->getById($id);

        if (!$album) {
            $this->flashMessage('Album now found');
        }

        $this->template->album = $album;
    }

    public function renderEdit($id)
    {
        $album = $this->albumRepository->getById($id);

        if (!$album) {
            $this->flashMessage('Album now found');
        }

        $this['albumForm']->setAlbum($album);
    }

    public function actionDelete($id, $artist_id = null)
    {
        $album = $this->albumRepository->getById($id);

        if ($album) {
            $this->albumRepository->delete($id);
            $this->flashMessage('Album as been deleted', 'alert-success');
        } else {
            $this->flashMessage('Album not found', 'alert-warning');
        }

        if ($artist_id) {
            $this->presenter->redirect("Artist:Show", ['id' =>$artist_id]);
        }
        $this->presenter->redirect('Homepage:Default');
    }

    protected function createComponentAlbumForm()
    {
        return new AlbumFormControl($this->albumRepository, $this->artistRepository->getAll()->fetchPairs('id', 'name'));
    }
}