<?php

namespace App\Presenters;

use App\Components\AlbumFormControl;
use App\Model\AlbumRepository;
use App\Model\ArtistRepository;
use App\Components\ArtistFormControl;

final class ArtistPresenter extends SecuredPresenter
{
    /** @var ArtistRepository @inject */
    public $artistRepository;

    /** @var AlbumRepository @inject */
    public $albumRepository;

    public function renderShow($id)
    {
        $artist = $this->artistRepository->getById($id);

        if (!$artist) {
            $this->flashMessage('Artist now found');
        }

        $this->template->artist = $artist;
        $this->template->albums = $artist->related('album');
    }

    public function renderEdit($id)
    {
        $artist = $this->artistRepository->getById($id);

        if (!$artist) {
            $this->flashMessage('Artist now found');
        }

        $this['artistForm']->setArtist($artist);
    }

    public function actionDelete($id)
    {
        $artist = $this->artistRepository->getById($id);

        if ($artist) {
            $this->artistRepository->delete($id);
            $this->presenter->flashMessage('Artist has been deleted', 'alert-success');
        } else {
            $this->flashMessage('Artist not found', 'alert-warning');
        }

        $this->presenter->redirect('Homepage:Default');
    }

    protected function createComponentArtistForm()
    {
        return new ArtistFormControl($this->artistRepository);
    }

    protected function createComponentAlbumForm()
    {
        return new AlbumFormControl($this->albumRepository, $this->artistRepository->getAll()->fetchPairs('id', 'name'));
    }

}