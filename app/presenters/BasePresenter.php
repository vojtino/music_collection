<?php

namespace App\Presenters;

use Nette;
use Nette\Security\User;

abstract class BasePresenter extends Nette\Application\UI\Presenter
{
    /** @var User @inject*/
    public $user;

    protected function beforeRender()
    {
        $this->template->userLoggedIn = false;
    }
}