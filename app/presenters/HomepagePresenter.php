<?php

namespace App\Presenters;

use App\Components\ArtistFormControl;
use App\Components\AlbumFormControl;
use App\Model\ArtistRepository;
use App\Model\AlbumRepository;


final class HomepagePresenter extends SecuredPresenter
{
    /** @var ArtistRepository @inject */
    public $artistRepository;

    /** @var AlbumRepository @inject */
    public $albumRepository;

    public function renderDefault()
    {
        $this->template->artists = $this->artistRepository->getAll();
        $this->template->albums = $this->albumRepository->getAll();
    }

    protected function createComponentArtistForm()
    {
        return new ArtistFormControl($this->artistRepository);
    }

    protected function createComponentAlbumForm()
    {
        return new AlbumFormControl($this->albumRepository, $this->artistRepository->getAll()->fetchPairs('id', 'name'));
    }
}
