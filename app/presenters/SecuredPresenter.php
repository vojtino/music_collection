<?php

namespace App\Presenters;

class SecuredPresenter extends BasePresenter
{
    protected function beforeRender()
    {
        if (!$this->user->isLoggedIn()) {
            $this->redirect('Sign:Default');
        }

        $this->template->userLoggedIn = true;
    }

    public function handleLogout()
    {
        $this->user->logout();
        $this->presenter->flashMessage('You have been logged out', 'alert-success');
    }
}