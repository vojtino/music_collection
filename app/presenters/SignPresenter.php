<?php

namespace App\Presenters;

use App\Components\LoginFormControl;
use App\Model\CredentialsAuthenticator;

final class SignPresenter extends BasePresenter
{
    /** @var CredentialsAuthenticator @inject */
    public $credentialsAuthenticator;

    /** @var LoginFormControl @inject */
    public $loginForm;

    protected function createComponentLoginForm()
    {
        return new LoginFormControl($this->user, $this->credentialsAuthenticator);
    }
}